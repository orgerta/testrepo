# Network Policies in AKS Cluster

## Table of Contents
1. [Prerequisites](#Prerequisites)
2. [First-deploy-pods-which-will-be-used-for-testing-network-policies](#First-deploy-pods-which-will-be-used-for-testing-network-policies)
3. [A-policy-to-every-Pod-in-the-namespace-only-allowing-Ingress-traffic-from-a-specific-IP-address-block](#A-policy-to-every-Pod-in-the-namespace-only-allowing-Ingress-traffic-from-a-specific-IP-address-block)
   1. [Create-a-yaml-file-called-allow-ingress.yaml](#Create-a-yaml-file-called-allow-ingress.yaml)
   2. [Apply-network-policy ](#Apply-network-policy )
   3. [Test-network-policy](#Test-network-policy)
4. [Allow-traffic-from-Pods-with-a-specific-label-that-exist-in-a-different-namespace](#Allow-traffic-from-Pods-with-a-specific-label-that-exist-in-a-different-namespace)
5. [Deny-all-traffic](#Deny-all-traffic)

If you want to control traffic flow at the IP address or port level (OSI layer 3 or 4), then you might consider using Kubernetes NetworkPolicies for particular applications in your cluster.


## Prerequisites

- Azure CLI version 2.0.61 or later installed and configured
- Network Plugin: 
AKS supports multiple network plugins, such as Azure CNI (Container Network Interface) and Kubenet. Azure CNI is recommended for advanced network policy functionality
- Network Policy Parameter:
1- Azure's own implementation, called Azure Network Policy Manager.
2- Calico Network Policies, an open-source network and network security solution



_Below are explained three kind of network policies_ 

### First-deploy-pods-which-will-be-used-for-testing-network-policies
Before deploying pods, we create two namespaces in our cluster
```sh
$kubectl create ns test 
$kubectl create ns dev
$kubectl label ns dev application=demo-app
```

```sh
apiVersion: v1
kind: Pod
metadata:
  name: frontend
  namespace: test
  labels:
    role: frontend
spec:
  containers:
  - name: nginx
    image: nginx
    ports:
    - name: http
      containerPort: 80
      protocol: TCP
---
apiVersion: v1
kind: Service
metadata:
  name: frontend-service
  namespace: test
  labels:
    role: frontend
spec:
  selector:
    role: frontend
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
---
apiVersion: v1
kind: Pod
metadata:
  name: backend
  namespace: test
  labels:
    role: backend
spec:
  containers:
  - name: nginx
    image: nginx
    ports:
    - name: http
      containerPort: 80
      protocol: TCP
---
apiVersion: v1
kind: Service
metadata:
  name: backend-service
  namespace: test
  labels:
    role: backend
spec:
  selector:
    role: backend
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
---
apiVersion: v1
kind: Pod
metadata:
  name: web
  namespace: dev
  labels:
    app: web
spec:
  containers:
  - name: nginx
    image: nginx
    ports:
    - name: http
      containerPort: 80
      protocol: TCP
---
apiVersion: v1
kind: Service
metadata:
  name: web-service
  namespace: dev
  labels:
    app: web
spec:
  selector:
    app: web
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
```
```sh
$ kubectl apply -f pods.yaml
```

### A-policy-to-every-Pod-in-the-namespace-only-allowing-Ingress-traffic-from-a-specific-IP-address-block
##### Create-a-yaml-file-called-allow-ingress.yaml




```sh
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: network-policy
  namespace: test
spec:
  podSelector: {}
  policyTypes:
    - Ingress
  ingress:
    - from:
        - ipBlock:
            cidr: 172.17.0.0/16
```


##### Apply-network-policy 
```sh
$ kubectl apply -f allow-ingress.yaml
networkingpolicy.networking.k8s.io/network-policy created
```
The empty podSelector block means all the namespace’s Pods are targeted by the policy. The ipBlock rule restricts ingress traffic to Pods with an IP address in the specified range. Egress traffic is not blocked.
##### Test-network-policy
To test network policy enter one of the pods. In this case lets enter frontend pod.From frontend pod try to communicate with backend pod.
```sh
$ kubectl exec -it fronted bash -n test
$ curl http://backend-service
```
If the backend pod's ip is inside the target range the output of the command will be:
```sh
$ kubectl exec -it fronted bash -n test
$ curl http://backend-service
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to Nginx</title>
</head>
<body>
    <h1>Welcome to Nginx</h1>
    <p>This is the default web page served by Nginx.</p>
</body>
</html>
```
Otherwise, if pod's ip is out of the specified range the curl command will give "Timed Out" as output



### Allow-traffic-from-Pods-with-a-specific-label-that-exist-in-a-different-namespace

##### Create-a-yaml-file-called-"allow-label-ingress.yaml"




```sh
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: network-policy-label
  namespace: test
spec:
  podSelector: {}
  policyTypes:
    - Ingress
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              application: demo-app
          podSelector:
            matchLabels:
              app: web
```


##### Apply-network-policy
```sh
$ kubectl apply -f allow-label-ingress.yaml
networkingpolicy.networking.k8s.io/network-policy-label created
```
The policy states that any Pod labelled app: web can reach all the Pods in the test namespace, if its own namespace is labelled demo-app.
##### Test-network-policy
To test network policy enter frontend pod in test namespace. From frontend pod try to retrieve info from web pod.
```sh
$ kubectl exec -it fronted bash -n test
$ curl http://web-service
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to Nginx</title>
</head>
<body>
    <h1>Welcome to Nginx</h1>
    <p>This is the default web page served by Nginx.</p>
</body>
</html>
```
In this case, frontend is in test namespace, dev namespace has a label "application: demo-app" and pod named web has a label "app: web",so it matches the criteria of network policy and nginx page info can be retrieved.



## Deny-all-traffic



```sh
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: deny-all
  namespace: test
spec:
  podSelector: {}
  policyTypes:
    - Ingress
    - Egress
```
```sh
$ kubectl apply -f deny-all.yaml
networkingpolicy.networking.k8s.io/deny-all created
```
This policy selects every Pod in the namespace and applies a rule that forbids all network communication

If i enter any pod in test namespace, and try to communicate or reach to any other pod i get  "Timed Out" as network policy blocks all ingress and egress traffic.We can choose to forbid only ingress or only egress traffic defining in configuration only one kind of policyTypes respectively.


